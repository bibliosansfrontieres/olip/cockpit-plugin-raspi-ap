/*
 * This file is part of Cockpit.
 *
 * Copyright (C) 2017 Red Hat, Inc.
 *
 * Cockpit is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * Cockpit is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Cockpit; If not, see <http://www.gnu.org/licenses/>.
 */

import cockpit from 'cockpit';
import React from 'react';
import './app.scss';

const _ = cockpit.gettext;

export class Application extends React.Component {
    constructor() {
        super();
        this.startHotspot = this.startHotspot.bind(this);
        this.stopHotspot = this.stopHotspot.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.refreshStatus = this.refreshStatus.bind(this);
        this.disabledEditHotspot = this.disabledEditHotspot.bind(this);
        this.applyChangePassword = this.applyChangePassword.bind(this);
        this.applyChangeSSID = this.applyChangeSSID.bind(this);

        this.state = {
            hostapdStatus: "UNKNOWN",
            inputNewPassword: "",
            inputNewSSID: "",
            wpa_passphrase: "",
            wpa_key_mgmt: "",
            ssid: "",
            hostapdConfFileContent: ""
        };
    }

    componentDidMount() {
        cockpit.file('/etc/hostapd/hostapd.conf').watch((content) => {
            if (content) {
                this.setState({ hostapdConfFileContent: content });

                let contentTable = content.split("\n");
                for (var i in contentTable) {
                    if (contentTable[i].split("=")[0] == "ssid") {
                        this.setState({ ssid: contentTable[i].split("=")[1] });
                    } else if (contentTable[i].split("=")[0] == "wpa_passphrase") {
                        this.setState({ wpa_passphrase: contentTable[i].split("=")[1] });
                    } else if (contentTable[i].split("=")[0] == "wpa_key_mgmt") {
                        this.setState({ wpa_key_mgmt: contentTable[i].split("=")[1] });
                    }
                }
            }
        });
        this.refreshStatus();
    }

    refreshStatus() {
        this.setState({ hostapdStatus: "REFRESHING" });

        setTimeout(() => {
            var proc = cockpit.spawn(["systemctl", "is-active", "hostapd"]);
            proc.stream((data) => {
                console.log("data stream:");
                console.log(data);
                
                if (data == "active\n") {
                    this.setState({ hostapdStatus: "UP" });
                    return "UP";
                } else {
                    this.setState({ hostapdStatus: "DOWN" });
                    return "DOWN";
                }
            });
        }, 3000)

    }

    handleInputChange(event) {
        const target = event.target;
        this.setState({
            [target.name]: target.value
        });
    }

    startHotspot() {
        var proc = cockpit.spawn(["systemctl", "start", "hostapd"]);
        proc.fail((e) => {
            this.refreshStatus();
        })
            .done((data) => {
                this.refreshStatus();
            });
    }

    stopHotspot() {
        var proc2 = cockpit.spawn(["systemctl", "stop", "hostapd"]);
        proc2.fail((e) => {
            console.log("stopHotspot() failed, error:");
            console.log(e);
            this.refreshStatus();
        })
            .done((data) => {
                this.refreshStatus();
            });
    }

    applyChangePassword() {
        cockpit.file('/etc/hostapd/hostapd.conf').modify((old_content) => {
            if (old_content) {
                let contentTable = old_content.split("\n");
                for (var i in contentTable) {
                    if (contentTable[i].split("=")[0] == "wpa_passphrase") {
                        contentTable[i] = "wpa_passphrase=" + this.state.inputNewPassword
                    }
                }
                return contentTable.join("\n");
            }
        })
    }

    applyChangeSSID() {
        cockpit.file('/etc/hostapd/hostapd.conf').modify((old_content) => {
            if (old_content) {
                let contentTable = old_content.split("\n");
                for (var i in contentTable) {
                    if (contentTable[i].split("=")[0] == "ssid") {
                        contentTable[i] = "ssid=" + this.state.inputNewSSID
                    }
                }
                return contentTable.join("\n");
            }
        })
    }

    disabledEditHotspot() {
        let status = this.state.hostapdStatus
        if (status == "REFRESHING" || status == "UP") {
            return true;
        } else {
            return false;
        }
    }

    disableNewPasswordButton() {
        let status = this.state.hostapdStatus
        if (status == "REFRESHING" || status == "UP" || this.state.inputNewPassword.length < 8) {
            return true;
        } else {
            return false;
        }
    }

    render() {
        return (
            <div className="container-fluid col-md-6 col-lg-5">
                <h2>Wifi Hotspot configuration</h2>
                <table className="table">
                    <tbody>
                        <tr>
                            <th>{cockpit.format(_("Hotspot status"))}</th>
                            <th>{cockpit.format(_("$0"), this.state.hostapdStatus)}</th>
                        </tr>
                        <tr>
                            <th>{cockpit.format(_("Wifi SSID (name)"))}</th>
                            <th>{cockpit.format(_("$0"), this.state.ssid)}</th>
                        </tr>
                        <tr>
                            <th>{cockpit.format(_("Wifi Passphrase"))}</th>
                            <th>{cockpit.format(_("$0"), this.state.wpa_passphrase)}</th>
                        </tr>
                        <tr>
                            <th>{cockpit.format(_("Wifi Security"))}</th>
                            <th>{cockpit.format(_("$0"), this.state.wpa_key_mgmt)}</th>
                        </tr>
                        <tr>
                            <th>Change Wifi SSID</th>
                            <th><input
                                name="inputNewSSID"
                                type="text"
                                disabled={this.disabledEditHotspot()}
                                value={this.state.inputNewSSID}
                                onChange={this.handleInputChange} />
                                <button
                                    onClick={this.applyChangeSSID}
                                    disabled={this.disabledEditHotspot()}>
                                    Apply
                            </button></th>
                        </tr>
                        <tr>
                            <th>Change password (Min. 8 Chars)</th>
                            <th><input
                                name="inputNewPassword"
                                type="text"
                                disabled={this.disabledEditHotspot()}
                                minLength={8}
                                maxLength={63}
                                value={this.state.inputNewPassword}
                                onChange={this.handleInputChange} />
                                <button
                                    disabled={this.disableNewPasswordButton()}
                                    onClick={this.applyChangePassword}>
                                    Apply
                            </button></th>
                        </tr>
                        <tr>
                            <th>Hotspot control</th>
                            <th><button
                                onClick={this.startHotspot}
                                disabled={this.disabledEditHotspot()}>
                                Start
                            </button>
                                <button
                                    onClick={this.stopHotspot}
                                    disabled={!this.disabledEditHotspot()}>
                                    Stop
                            </button></th>
                        </tr>
                    </tbody>
                </table>
                New password len : {this.state.inputNewPassword.length}
            </div>
        );
    }
}
